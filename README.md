# README #

If you like brickwork, this program is for you!
It is a database of brickwork patterns, which are printed on screen.
You can also let it print you own patterns, using a coding system

### What is this repository for? ###

* prints brickwork patterns from the (Dutch or American) name of the pattern
* also does "wild" pattern, following the rules of the trade
* sorry, not yet ported to python3

### How do I get set up? ###

* It is a single file program; -h gives help
* use -e cp850 or -e utf8 to get name blocks instead of an ASCII approximation

### Contribution guidelines ###

* You're welcome to participate, especially if you know a pattern that isn't in the program!

### Who do I talk to? ###

* jnebos, at the google based mail server