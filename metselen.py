#!python2
"""Brick pattern generator.
Draws a wall of few known and lesser known brick patterns.
There is a code to define your own patterns.
If you see a pattern somewhere not in the list, mail me at jnebos@gmail.com.
"""
#Version 0.1: initial commit and publication
#Version 0.2: improved help message a bit, added version log
#Version 0.2.1: intermediate
#Version 0.3: with backtracking wildverband
#Version 0.4: with argparse
#Version 0.5: translated, with partial pattern match
#Version 0.6: added Uitgeest pattern
VERSION = '0.6'
syntaxBeschrijving = '''
A brick pattern is described row by row.
Row descriptions may be separated by whitespace for legibility.
A row descriptor is:
    [+][displacement]<pattern or %>
+: displacement zero, but displace all following rows from now on
displacement: shift the pattern to the right by so many
    quarter stretchers (in other words, half headers)
pattern: any combination of
    'k' (header, for 'kop' in Dutch)
    's' (stretcher, for 'strek' in Dutch)
    this pattern of bricks is repeated until the row is full
%: same pattern as previous row
The rows are repeated cyclically until the height is reached.
'''

import re, random, unicodedata, sys, collections
from operator import methodcaller
#random.seed(7)

kop, strek = '|_', '|___'

#---------printing---------------------------------------------
class ascii:
    downline = horline = '_'
    bothline = upline = '|'

class utf8:
    horline=u'\N{box drawings heavy horizontal}'
    upline=u'\N{box drawings heavy up and horizontal}'
    downline=u'\N{box drawings heavy down and horizontal}'
    bothline=u'\N{box drawings heavy vertical and horizontal}'

class cp850:
    def makeLight(c):
        "Make a character from 'heavy' to 'light"
        return unicodedata.lookup(unicodedata.name(c).replace('HEAVY','light'))

    horline, upline, downline, bothline = map(makeLight,
        (utf8.horline, utf8.upline, utf8.downline, utf8.bothline))

lastLine = ''

def printEncoded(s, code):
    """Print a string using appropriate graphics"""
    global lastLine
    #last line
    if s is None: s = lastLine.replace('|', '_')
    #fill line if it is shorter than previous
    if len(lastLine)<len(s): lastLine += '_'*(len(s)-len(lastLine))
    #fill in the graphics
    result = ''
    for i,c in enumerate(s):
        if c=='|':
            if lastLine[i]=='|': result += code.bothline
            else: result += code.downline
        elif c!='_': result += c
        elif lastLine[i]=='|': result += code.upline
        else: result += code.horline
    print result.encode(code.__name__)
    lastLine = s

#---------standard patterns-------------------------------------

#TODO: explain origin of patterns with the --list option
examples = {
    #traditional Dutch
    'halfsteens': '+2s',    'stretcher': '+2s',
    'koppen':     '+1k',    'header': '+1k',
    'kruis':      '1k +2s', 'dutch': '+1k', 'english cross': '+1k',
    'staand':     's 1k',   'english': '+1k',
    'zaans':      '+3ks',   'flemish': '+3ks',
    'klezoor':    's 1s',
    'ketting':    '+5kss',  'monk': '+5kss',
    'engels':     '+7ksss', 'flemish garden wall': '+7ksss',
    'frans':      'kks 3%',
    'blok':       '+2s %',
    #American
    'diamond':    'ksss5%12%7%0%9%12%7%2%5%0%7%2%9%',
    'amerikaans': 's2s0s2s0s2s1k',  'american': 's2s0s2s0s2s1k',
    #metselen.be
    'tuinmuur':   's1k 2s', 'garden wall': 's1k 2s',
    'ruit':       'kssss11%2%9%+9%',
    'kropholler': '+3kss',  'deltrap': '+3kss',
    'tegel':      's',      'stack': 's',
    'korver':     'kss 1s',
    #ibstock.com technical information sheet 21
    'diamant':    '+4k 3kks 0k 3kks', 'diamond diaper': '+4k 3kks 0k 3kks',
    #Found myself in Denmark
    'esbjerg':    'kss 3% 8% 5%',
    'ribe':       'kss7%4%7%%3%',
    'varde':      'ksss7%4%11%',
    'zigzag':     'kss 3s 2kss 1s',
    #Found myself in the Netherlands
    'veldhoven':  '+3ksss',
    'heemskerk':  '+1s',
    'uitgeest':   's3sk',
    #Invented myself
    'tam':        '+7kssksss', 'tame': '+7kssksss',
    }

def matchPattern(name):
    "Check if a name defines a unique pattern"
    #match beginning of word, or part of word if nothing matches
    matches = filter(methodcaller('startswith', name), examples.keys()) or \
        filter(methodcaller('__contains__', name), examples.keys())
    if len(matches)==1: return matches[0]
    return None

def listPatterns():
    print 'List of standard patterns:'
    patList = collections.defaultdict(list)
    for name, pat in examples.items():
        patList[pat].append(name)
    print 'code'.rjust(25)+' ', 'name'
    for pat, names in sorted(patList.items()):
        print pat.rjust(25)+':', ','.join(sorted(names))

patternRow = r'(\+?)([0-9]*)([ks]+|%)\s*'
#match a row of the pattern
rowPat = re.compile(patternRow)
#match the full pattern
patternPat = re.compile('('+patternRow+')+$')

def printPattern(pattern, height=15, width=70):
    """Print the brick pattern."""
    assert patternPat.match(pattern), 'Pattern syntax error: '+pattern
    unpacked = rowPat.findall(pattern)
    lineNumber = 0
    patternDisplacement = 0
    lastRow = 's'
    for lineNumber in range(height):
        plus, displacement, row = unpacked[lineNumber%len(unpacked)]
        if row == '%': row = lastRow
        else: lastRow = row
        #compute the displacement to the right
        displacement = int(displacement or '0')
        if plus:
            #relative displacement
            patternDisplacement += displacement
            #compensate for adjusting patternDisplacement too early
            displacement = -displacement
        displacement += patternDisplacement
        row = ''.join({'k': kop, 's': strek}[l] for l in row)
        #compute amount to cut away
        displacement = -displacement%len(row)
        #repeat pattern often enough
        row *= (width+displacement)//len(row)+1
        printer( row[displacement:width+displacement] )
    printer()

# ----------- "wild" bond (wildverband in Dutch) ------------------
#parameters for wildverband: very restricted variant, combined from:
#  standard according to the masonry rules: 4, 2, 6, 6
#  masonry company "strijker" te Drenthe: 5, 3, 7, 5
maxStretchers = 4
maxHeaders = 2
maxBrickStack = 6
maxFalling = 5
forbidDiagonalHeaders = False  #works only for OptimisedBrickRow
#headers per surface area is about .16+.32*chooseHeaderFirst
#.98:47% .9:41%, .8:37%, .7: 34%, .6:31%, .5:29% .4:27%
chooseHeaderFirst = .2

class BackTracker:
    "Base class for a decent backtracker."
    backtrackDepth = 1000
    def __init__(self):
        "Build an empty object here"

    def possibilities(self):
        "Give possibilities to add something"

    def addThing(self, thing):
        "Add something from total to the object"

    def removeThing(self):
        "Remove something: inverse of addThing"

    def useable(self):
        "Flag if something is ready for outputting"

    def backtrack(self):
        """For educational purposes.
        We use the slightly faster method below"""
        if self.useable():
            yield self
            return
        for thing in self.possibilities():
            self.addThing(thing)
            for result in self.backtrack(): yield result
            self.removeThing()

    def backtrack(self):
        """The main backtrack loop.
        Equivalent to routine above, but slightly faster,
        with fixed stack space, and a max recovery depth.
        """
        self.generators = []
        #invariant: elements in result have an equivalent enty in generators
        while 1:
            if self.useable():
                #ready to output
                yield self
            else:
                #not yet; add generator
                nextgen = self.possibilities()
                try:
                    #check if generator works
                    self.addThing(nextgen.next())
                    #it works: add to generators and object
                    self.generators.append(nextgen)
                    #success: back to invariant, no need to step gen
                    #limit the amount of backtracking
                    if len(self.generators)>self.backtrackDepth:
                        self.generators.pop(0)
                    continue
                except StopIteration, message:
                    #no object from generator, so no need to add
                    pass
            #step the last generator
            while self.generators:
                self.removeThing()
                try:
                    object = self.generators[-1].next()
                    #succes
                    self.addThing(object)
                    break
                except StopIteration:
                    #generator ran out: step previous level
                    self.generators.pop()
            else:
                #no more generators
                return

class BrickRow(BackTracker):
    """Row of bricks.
        The row is stored as an array of booleans for an opening between
            bricks. E.g. a header and a stretcher are TFTFFFT
            The leftmost T at position 0 is the beginning of the bricks.
        oddBrick: set to 3 if you want a three quarter brick at left.
        lTeeth[i]: indicates the number of "falling teeth" to left up
            starting at point i (that is, crossing i-1 line of this row)
        rTeeth[i]: same for left up, crossing i+1 line
        brickStack: the number of headers stacked with offset 1/4.
            defined for the middle of the brick, 0 at the edges
    """
    def __init__(self, length, oddBrick, rTeeth, lTeeth, brickStack):
        self.backtrackDepth = 7 # if lookahead = True, only 5 is needed
        #length of row in quarter bricks
        self.length = length
        #add some extra data to make computation of possibilities easier
        self.rTeeth = rTeeth + 4*[0]
        self.lTeeth = lTeeth + 4*[0]
        self.brickStack = brickStack + 4*[0]
        self.row = ''
        #number of quarter brick at start/end
        self.startQ = 3 if oddBrick else 0
        self.endQ = 3 if (length-self.startQ)%2 else 0

    def addThing(self, brick):
        self.row += brick

    def removeThing(self):
        self.row = self.row[:-1]

    def __len__(self):
        "length of bricks excl ends"
        l = len(self.row)+self.row.count('s')
        return 2*l

    def useable(self):
        return len(self)==self.length-self.startQ-self.endQ

    def possibilities(self):
        "Find all possibilties for adding a brick."
        pos = len(self)+self.startQ
        #get the needed variables
        brickStack = self.brickStack
        lTeeth = self.lTeeth
        rTeeth = self.rTeeth
        selfRowEndswith = self.row.endswith
        startQ, endQ = self.startQ, self.endQ
        #we are already in trouble that we can't fix (edge problem)
        if brickStack[pos+1]>maxBrickStack \
                or brickStack[pos+3]>maxBrickStack \
                or rTeeth[pos+1]>=maxFalling:
            return
        #prevent immediate breaking of the rules
        noHeader = lTeeth[pos+1]>=maxFalling \
                or rTeeth[pos+3]>=maxFalling \
                or selfRowEndswith('h'*maxHeaders)
        noStretcher = selfRowEndswith('s'*maxStretchers) \
                or lTeeth[pos+3]>=maxFalling \
                or rTeeth[pos+5]>=maxFalling \
                or brickStack[pos+1]>=maxBrickStack \
                or brickStack[pos+3]>=maxBrickStack \
                or pos+4+endQ>self.length

        #shuffle the possibilities
        if random.random()<chooseHeaderFirst:
            if headerOK: yield 'h'
            if stretcherOK: yield 's'
        else:
            if stretcherOK: yield 's'
            if headerOK: yield 'h'

    def output(self):
        result = [True]
        if self.startQ: result.extend([False, False, True])
        for b in self.row:
            if b=='h': result.extend([False, True])
            else: result.extend([False, False, False, True])
        if self.endQ: result.extend([False, False, True])
        return result

    def __str__(self):
        return ''.join('|' if b else '_' for b in self.output())

    @staticmethod
    def p(data):
        "Handy debug routine to print lTeeth, rTeeth, brickStack"
        return ''.join(
            {None: ' ', False: '_'}.get(s, str(s))
            for s in data)

class OptimisedBrickRow(BrickRow):
    """Row of bricks.
    Optimisation of the BrickRow class, where the possibilities routine does
    much effort to prevent future rows from getting stuck.
    """
    paranoid = False
    def possibilities(self):
        """Find all possibilties for adding a brick.
        This version does many additional tests to prevent getting stuck
        on the next row."""
        pos = len(self)+self.startQ
        headerOK = stretcherOK = True
        #get the needed variables
        brickStack = self.brickStack
        bsm3, bsm1, bs1, bs3 = (maxBrickStack - brickStack[pos+i]
            for i in (-3, -1, 1, 3))
        lTeeth = self.lTeeth
        ltm3, ltm1, lt1, lt3, lt5 = (maxFalling - lTeeth[pos+i]
            for i in (-3, -1, 1, 3, 5))
        rTeeth = self.rTeeth
        rt1, rt3, rt5 = (maxFalling - rTeeth[pos+i]
            for i in (1, 3, 5))
        selfRowEndswith = self.row.endswith
        length = self.length
        startQ, endQ = self.startQ, self.endQ

        #trouble we can't fix (edge problem)
        if bs1<0 or bs3<0 or rt1<=0: return
        if self.paranoid:
            assert pos+1<length, str(pos)
            assert min(ltm1, lt1, lt3, lt5)>=0, str(pos)
            assert min(rt1, rt3, rt5)>=0, str(pos)
            assert min(bsm3, bsm1, bs1, bs3)>=0, str(pos)

        #prevent immediate breaking of the rules
        if lt1<=0 or rt3<=0: headerOK = False       #pos 2, falling teeth
        if lt3<=0 or rt5<=0: stretcherOK = False    #pos 4, falling teeth
        if bs1<=0 or bs3<=0: stretcherOK = False    #pos 2, brick stack
        if selfRowEndswith('s'*maxStretchers): stretcherOK = False
        if selfRowEndswith('h'*maxHeaders): headerOK = False
        if pos+4+endQ>length: stretcherOK = False

        #situations where the next row would go into trouble
        #prevent two almost max successive falling teeth in next row
        if lt1<=1 and rt3<=1: headerOK = False      #pos 3 and 1
        if ltm1<=1 and rt5<=1: stretcherOK = False  #pos 1 and 3
        if selfRowEndswith('h') and ltm3<=1 and rt3<=1:
            headerOK = False     #pos -1 and 1
        if rt1<=1 and rt3<=1: headerOK = False      #pos -1 and 1
        if ltm1<=1 and lt1<=1: headerOK = False     #pos 1 and 3
        #prevent three almost max brickStack in next row
        if selfRowEndswith('s') and bsm3<=1 and bsm1<=1 and bs1<=1:
            stretcherOK = False                     #pos -2 and 0
        #prevent brickStack at edge
        if startQ==0 and pos==0 and bs1<=2: stretcherOK = False
        if endQ==0 and pos+4==length and bs3<=2:
            stretcherOK = False
        #prevent impossible gap on next row, pos 1 or 3
        if (ltm1<=1 or rt5<=1) and (bs1<=1 or bs3<=1):
            stretcherOK = False
        #prevent falling teeth forcing edge of wall
        if endQ==0 and pos+6==length and lt1<=2:
            headerOK = False
        if endQ==0 and pos+8==length and lt3<=2:
            stretcherOK = False
        if startQ==0 and pos==0 and rt5<=2: stretcherOK = False
        if startQ==0 and pos==2 and rt3<=2: headerOK = False

        #expertimental: forbid diagonal headers
        if forbidDiagonalHeaders and lt1<5 and (ltm1<5 or lt3<5):
            headerOK = False

        #shuffle the possibilities
        if random.random()<chooseHeaderFirst:
            if headerOK: yield 'h'
            if stretcherOK: yield 's'
        else:
            if stretcherOK: yield 's'
            if headerOK: yield 'h'

class Wall(BackTracker):
    """A wall."""
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.rows = []
        counters = [None,0]*(self.width//2+1)
        self.lTeeth = [counters[:self.width+1]]
        self.rTeeth = self.lTeeth[:]
        self.brickStack = self.lTeeth[:]

    def addThing(self, row):
        output = row.output()
        slt1 = self.lTeeth[-1]+[0] #extra entry self.width+1
        lTeeth = [None]*(self.width+1)
        srt1 = self.rTeeth[-1]+[0] #extra entry -1
        rTeeth = lTeeth[:]
        sb1 = self.brickStack[-1]+[0]  #extra entry both -1 and self.width+1
        brickStack = lTeeth[:]
        for i in range(len(self.rows)%2,self.width+1,2):
            lTeeth[i] = output[i] and slt1[i-1]+1
            rTeeth[i] = output[i] and srt1[i+1]+1
            brickStack[i] = not output[i] and 1+max(sb1[i-1], sb1[i+1])
        self.rows.append(row)
        self.lTeeth.append(lTeeth)
        self.rTeeth.append(rTeeth)
        self.brickStack.append(brickStack)

    def removeThing(self):
        del self.rows[-1]
        del self.lTeeth[-1]
        del self.rTeeth[-1]
        del self.brickStack[-1]

    def useable(self):
        return len(self.rows)>=self.height

    def possibilities(self):
        for i in range(10):
            yield OptimisedBrickRow(length = self.width,
                oddBrick = len(self.rows)%2,
                lTeeth = self.lTeeth[-1],
                rTeeth = self.rTeeth[-1],
                brickStack = self.brickStack[-1]).backtrack().next()

    def __repr__(self):
        return '\n'.join(map(str, self.rows))

class EarlyPrintWall(Wall):
    "Like Wall, but begins printing lines as soon as they are stable."
    def __init__(self, height, width):
        Wall.__init__(self, height, width)
        self.backtrackDepth = 4
        self.printed = -1

    def addThing(self, row):
        Wall.addThing(self, row)
        if len(self.rows)-5>self.printed:
            printer(str(self.rows[-5]))
            self.printed = len(self.rows)-5

def printwild(height=30, width=100):
    wall = EarlyPrintWall(height, width).backtrack().next()
    for row in wall.rows[wall.printed+1:]: printer(str(row))
    printer()

# ---- main program, with argument parsing ------------------
if __name__=='__main__':
    def patternType(string):
        if patternPat.match(string) or \
            string=='wild' or \
            matchPattern(string): return string
        raise argparse.ArgumentTypeError(
            "can't interpret as pattern or pattern name: "+string)

    import argparse
    parser = argparse.ArgumentParser(
        description='This program prints brick patterns.',
        epilog='Standard patterns: '+', '.join(sorted(
            examples.keys()+['wild'])),
        )

    parser.add_argument('-c', '--columns', action='store', default=78,
        metavar='C', type=int,
        help='print %(metavar)s chars per line (default: %(default)s)')
    parser.add_argument('-l', '--lines', action='store', default=24,
        metavar='R', type=int,
        help='print %(metavar)s lines (default: %(default)s)')
    parser.add_argument('-e', '--encode', action='store', default='ascii',
        metavar = 'CODE', type=str, help='encode output: %(choices)s',
        choices=('ascii','cp850','utf8'))
    parser.add_argument('-s', '--syntax', action='store_true', default=False,
        help='explain pattern syntax')
    parser.add_argument('--list', action='store_true', default=False,
        help='list all standard patterns with encoding')
    parser.add_argument('-v', '--version', action='version', version=VERSION)
    parser.add_argument('pattern', nargs='?', action='store',
        type=patternType, help='brick pattern to print: encoded pattern or '
            'standard pattern from list')

    args = parser.parse_args()

    def printer(s=None):
        printEncoded(s, globals()[args.encode])

    if args.syntax:
        print syntaxBeschrijving
    elif args.list:
        listPatterns()
    elif not args.pattern:
        parser.print_usage()
    elif args.pattern=='wild':
        printwild(args.lines, args.columns)
    else:
        if not patternPat.match(args.pattern):
            pattern = matchPattern(args.pattern)
            print "Brick pattern name:", pattern
            pattern = examples[pattern]
        else:
            pattern = args.pattern
        printPattern(pattern, args.lines, args.columns)
